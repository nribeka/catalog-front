export const DEFAULT_PAGE_SIZE = 20

export const DisplayType = {
  LIST: 'list',
  CARD: 'card'
}

export const ObjectType = {
  PRODUCT: 'PRODUCT',
  PROJECT: 'PROJECT',
  ORGANIZATION: 'ORGANIZATION',
  OPEN_DATA: 'OPEN_DATA',
  USE_CASE: 'USE_CASE',
  BUILDING_BLOCK: 'BUILDING_BLOCK',
  PLAYBOOK: 'PLAYBOOK'
}
