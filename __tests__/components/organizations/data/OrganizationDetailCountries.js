module.exports = {
  'countries': {
    'data': {
      'countries': [
        {
          'id': 2,
          'name': 'Another Country',
          'slug': 'ac',
          '__typename': 'Country'
        },
      ]
    }
  }
}
