export const organizations = {
  data: {
    organizations: [
      {
        id: 2,
        name: 'Another Organization',
        slug: 'ao'
      }
    ]
  }
}
