export const buildingBlock = {
  id: 1,
  name: 'Test buildingBlock',
  slug: 'test_buildingBlock',
  products: [
    {
      id: 1,
      name: 'Product 1',
      slug: 'product_1'
    }
  ],
  productsMappingStatus: 'VALIDATED'
}
