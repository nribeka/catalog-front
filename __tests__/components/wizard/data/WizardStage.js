export const projData = {
  sectors: [
    {
      slug: 'sector1',
      name: 'Sector1',
      subSectors: 'Sector1'
    },
    {
      name: 'Sector2',
      subSectors: 'Sector2'
    }
  ],
  useCases: [
    {
      slug: 'use_case1',
      name: 'UseCase1',
    }
  ],
  countries: [
    {
      slug: 'country1',
      name: 'Country1',
    }
  ],
  mobileServices: [''],
  tags: [
    {
      slug: 'tag1',
      name: 'Tag1',
    }
  ],
  buildingBlocks: ['Data collection', 'Registration']
}

export const allValues = {
  projectPhase: 'projectPhase',
  sector: 'sector',
  subsector: 'subsector',
  sdg: 'Sdg1',
  useCase: '',
  countries: [],
  tags: [],
  mobileServices: [],
  buildingBlocks: ['buildingBlocks'],
  productSortHint: '',
  projectSortHint: ''
}
